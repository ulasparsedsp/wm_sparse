/** Implementation file for wmedian.hpp C++ header
 *  You must install Armadillo C++ linear algebra library to use this
 *  functions.
 *  Armadillo is available at http://arma.sourceforge.net/
 */

#include <armadillo>
#include "wmedian.hpp"

using namespace arma;


double wmedian(vec& samples, vec& weights) {
    /** Traditional Weighted Median Operator with real weight function
     * 
     *  X = WMEDIAN(SAMPLES, WEIGHTS) returns the weighted median of SAMPLES
     *  weighted by WEIGHTS.
     *   
     *  Reference:
     *  A General Weighted Median Filter Structure Admitting Negative
     *  Weights
     *  http://dx.doi.org/10.1109/78.735296
     */
     
    vec signedSamples = samples % (sign(weights) + (weights == 0));
    vec usweights = abs(weights);
    uvec sortedIndex = sort_index(signedSamples, "descend");
    double threshold = sum(usweights)/2.0;
    
    double partialSum = 0;
    uword i = 0;
    
    while (partialSum < threshold) {
        partialSum += usweights.at(sortedIndex.at(i));
        i++;
    }
    
    return signedSamples.at(sortedIndex.at(i - 1));
}


double wmedianf(vec& samples, vec& weights) {
    /** Fast Weighted Median Operator with real weight function
     * 
     *  X = WMEDIAN(SAMPLES, WEIGHTS) returns the weighted median of SAMPLES
     *  weighted by WEIGHTS.
     */
     
    uword N = samples.n_elem;
    vec signedSamples = samples % (sign(weights) + (weights == 0));;
    vec usweights = abs(weights);
    uvec sortedIndex = sort_index(signedSamples, "descend");
    double pivot = 0;
    double threshold = 0;
    double w1 = 0;
    double w2 = 0;
    uword N1 = 0;
    
    uword i;
    for (i = 0; i < N; i++) {
        pivot += signedSamples.at(i);
        threshold += usweights.at(i);
    }
    pivot /= N;
    threshold /= 2.0;
    
    for (i = 0; i < N; i++) {
        if (signedSamples.at(i) > pivot) {
            w1 += usweights.at(i);
            N1++;
        } else {
            w2 += usweights.at(i);
        }
    }
    
    double partialSum = 0;
    
    if (w1 > w2) {
        i = N1;
        partialSum = w1;
        
        while (partialSum >= threshold) {
            i--;
            partialSum -= usweights.at(sortedIndex.at(i));
        }
    } else {
        i = N1 - 1;
        partialSum = w1;
        
        while (partialSum < threshold) {
            i++;
            partialSum += usweights.at(sortedIndex.at(i));
        }
    }

    return signedSamples.at(sortedIndex.at(i));
}
