/** Implementation file for wmr.hpp C++ header
 *  You must install Armadillo C++ linear algebra library to use this
 *  functions.
 *  Armadillo is available at http://arma.sourceforge.net/
 */

#include <iostream>
#include <string>
#include <armadillo>
#include "wmr.hpp"
#include "wmedian.hpp"

using namespace arma;


vec wmr(vec& y, mat& A, double beta, double tol, int itmax) {
    /** Traditional solver for the l0-LAD optimization problem based on the
     *  Weighted Median operator
     *
     *  x = arg min ||y - Ax||_1 + lambda ||x||_0
     *
     *  X = WMR(Y, A, BETA, TOL, ITMAX) returns a A.n_rows-element
     *  coefficient vector that represents vector Y on the vector space of A.
     */
    
    uword M = A.n_rows;
    uword N = A.n_cols;
    double error = 1;
    double thr = max(abs(A.t() * y));
    vec x = zeros<vec>(N);
    vec yaux = zeros<vec>(M);
    double n2Y = pow(norm(y), 2.0);
    uword iter = 0;
    uword k;
    double xTemp, xprev, rt;
    vec rn(M);
    vec w(M);
    vec samples;
    
    // Outer loop
    while ((iter < itmax) && (error > tol)) {
        // Inner loop
        for (k = 0; k < N; k++) {
            rn = y - yaux + (A.col(k) * x.at(k));
            w = abs(A.col(k));
            samples = rn/A.col(k);
            xTemp = wmedianf(samples, w);
            xprev = x.at(k);
            rt = sum(abs(rn) - abs(rn - A.col(k) * xTemp));
            x.at(k) = (double)(rt > thr) * xTemp;
            yaux += A.col(k) * (x.at(k) - xprev);
        }
        
        error = pow(norm(y - yaux), 2.0) / n2Y;
        iter++;
        thr *= beta;
    }
    
    return x;
}


vec arwmr(vec& y, mat& A, double epsilon, double beta, double tol, int itmax) {
    /**
     *
     */
    uword M = A.n_rows;
    uword N = A.n_cols;
    vec x = zeros<vec>(N);
    mat W = abs(A);
    double error = 1;
    double tau = epsilon * sum(W).max()/2.0;
    vec yaux = zeros<vec>(M);
    double n2Y = pow(norm(y), 2.0);
    uword iter = 0;
    uword k;
    vec Yi = zeros<vec>(M+1);
    vec wi(M+1);
    double xprev;
    
    // Outer Loop
    while ((iter < itmax) && (error > tol)) {
        // Inner loop
        for (k = 0; k < N; k++) {
            Yi(span(0, M-1)) = (y - yaux + A.col(k) * x.at(k)) / A.col(k);
            wi(span(0, M-1)) = W.col(k);
            wi.at(M) = tau / (epsilon + abs(x.at(k)));
            xprev = x.at(k);
            x.at(k) = wmedianf(Yi, wi);
            yaux += A.col(k) * (x.at(k) - xprev);
        }
        
        error = pow(norm(y - yaux), 2.0) / n2Y;
        iter++;
        tau *= beta;
    }
    
    return x;    
}

vec rswmr(vec& y, mat& A, int S, double epsilon, double beta, double tol,
          int itmax, std::string infunc, double infunc_k)
{
    /*
     *
     */
     
    uword M = A.n_rows;
    uword N = A.n_cols;
    vec x = zeros<vec>(N);
    mat W = abs(A);
    double error = 1;
    double tau = epsilon * sum(W).max()/2.0;
    vec yaux = zeros<vec>(M);
    double n2Y = pow(norm(y), 2.0);
    uword iter = 0;
    uword k;
    vec e, e_psi;
    uvec indexes, T;
    vec Yi = zeros<vec>(M+1);
    vec wi(M+1);
    double xprev;
    psiFun func = psi_fun(infunc);
    
    // Outer Loop
    while ((iter < itmax) && (error > tol)) {
        e = y - yaux;
        e_psi = func(e, infunc_k);
        indexes = sort_index(abs(A.t() * e_psi), "descend");
        
        if (T.is_empty()) {
            T = sort(indexes(span(0, 2*S - 1)));
        } else {
            T = unique(join_vert(T,indexes(span(0, S - 1))));
        }
        
        // Inner loop
        for (k = 0; k < T.n_elem; k++) {
            Yi(span(0, M-1)) = (y - yaux + A.col(T.at(k)) * x.at(T.at(k))) / A.col(T.at(k));
            wi(span(0, M-1)) = W.col(T.at(k));
            wi.at(M) = tau / (epsilon + abs(x.at(T.at(k))));
            xprev = x.at(T.at(k));
            x.at(T.at(k)) = wmedianf(Yi, wi);
            yaux += A.col(T.at(k)) * (x.at(T.at(k)) - xprev);
        }
        
        indexes = sort_index(abs(x), "descend");
        yaux.fill(0);
        for (k = 0; k < S; k++) {
            yaux += A.col(indexes.at(k)) * x.at(indexes.at(k));
        }
        x(indexes(span(S,2*S - 1))).fill(0);
        T = indexes(span(0, S-1));
        error = pow(norm(y - yaux), 2.0) / n2Y;
        iter++;
        tau *= beta;
   }
   
   return x;
}

vec cauchy(vec& e, double k) {
    vec y = pow(k, 2) * 2 * e / (pow(k, 2) + pow(e, 2));
    return y;
}

vec laplace(vec& e, double k) {
    vec y = sign(e);
    return y;
}

vec huber(vec& e, double k) {
    vec e_ones = ones<vec>(e.n_elem);
    vec y = e % min(1.345/abs(e), e_ones);
    return y;
}

vec bisquare(vec& e, double k) {
    vec e_zeros = zeros<vec>(e.n_elem);
    vec y = e % pow(max(e_zeros, 1-(pow(e, 2)/pow(4.685, 2))), 2);
    return y;
}

psiFun psi_fun(std::string funName) {
    if (funName == "cauchy") {
        return cauchy;
    } else if (funName == "laplace") {
        return laplace;
    } else if (funName == "huber") {
        return huber;
    } else if (funName == "bisquare") {
        return bisquare;
    }
}
