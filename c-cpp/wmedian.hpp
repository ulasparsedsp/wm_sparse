/** Armadillo-based Weighted Median Operator functions for C++ programs
 */

#ifndef WMEDIAN_H
#define WMEDIAN_H

#include <armadillo>


double wmedian(arma::vec& samples, arma::vec& weights);

double wmedianf(arma::vec& samples, arma::vec& weights);


#endif // WMEDIAN_H included
