#ifndef WMR_H
#define WMR_H

#include <string>
#include <armadillo>


arma::vec wmr(arma::vec& y, arma::mat& A, double beta = 0.95, double tol = 1e-6,
              int itmax = 15);

arma::vec arwmr(arma::vec& y, arma::mat& A, double epsilon = 0.01,
                double beta = 0.95, double tol = 1e-6, int itmax = 15);

arma::vec rswmr(arma::vec& y, arma::mat& A, int S, double epsilon = 0.01,
                double beta = 0.95, double tol = 1e-6, int itmax = 15,
                std::string infunc = "cauchy", double infunc_k = 0.25);
                
arma::vec cauchy(arma::vec& e, double k = 0.25);

arma::vec laplace(arma::vec& e, double k = 1);

arma::vec huber(arma::vec& e, double k = 1);

arma::vec bisquare(arma::vec& e, double k = 1);

typedef arma::vec (*psiFun)(arma::vec&, double);

psiFun psi_fun(std::string funName);


#endif // WMR included
