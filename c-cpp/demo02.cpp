/** Demo for Algorithms based on the Weighted Median Regression
 *  listed in wmr.hpp
 *
 *  Compile with:
 *  g++ demo02.cpp
 */

#include <iostream>
#include <armadillo>
#include "wmr.hpp"

using namespace std;
using namespace arma;

int main(){
    arma_rng::set_seed_random();
    wall_clock timer;
    
    uword N = 512;
    uword M = 256;
    uword S = 8;
    
    vec x = zeros<vec>(N);
    vec coeff = randn<vec>(S);
    uvec coeffIndex = shuffle(linspace<uvec>(0, N-1, N));
    x(coeffIndex(span(0, S-1))) = coeff;
    
    mat A = randn<mat>(M, N);
    A *= diagmat(1/sqrt(sum(square(A))));
    
    vec y = A * x;
    
    // WMR
    timer.tic();
    vec xHat = wmr(y, A);
    double time_wmr = timer.toc();
    
    // ARWMR
    timer.tic();
    vec xHat2 = arwmr(y, A);
    double time_arwmr = timer.toc();
   
    // RSWMR 
    timer.tic();
    vec xHat3 = rswmr(y, A, S);
    double time_rswmr = timer.toc();
    
    
    cout << "\nCoefficients recovery test at actual positions:\n" << endl;
    cout << "  Coeff    WMR      ARWMR    RSWMR\n";
    cout << join_horiz(join_horiz(join_horiz(coeff, xHat(coeffIndex(span(0, S-1)))), xHat2(coeffIndex(span(0, S-1)))), xHat3(coeffIndex(span(0, S-1)))) << endl;
    cout << "WMR elapsed time = \t" << scientific << time_wmr << " s\n";
    cout << "No. recovered: " << accu(xHat != 0) << "\n";
    cout << "MSE = " << 1.0/N * sum(pow(x - xHat, 2)) << "\n\n";
    cout << "ARWMR elapsed time:\t" << scientific << time_arwmr << " s\n";
    cout << "No. recovered: " << accu(xHat2 != 0) << "\n";
    cout << "MSE = " << 1.0/N * sum(pow(x - xHat2, 2)) << "\n\n";
    cout << "RSWMR elapsed time:\t" << scientific << time_rswmr << " s\n";
    cout << "No. recovered: " << accu(xHat3 != 0) << "\n";
    cout << "MSE = " << 1.0/N * sum(pow(x - xHat3, 2)) << "\n" << endl;
    return 0;
}
