/** Demo for Weighted Median Operator functions
 *  listed in wmedian.hpp
 *
 *  Compile with:
 *  g++ demo01.cpp wmedian.cpp wmedian.hpp -o demo01 -O2 -larmadillo
 */

#include <iostream>
#include <armadillo>
#include "wmedian.hpp"

using namespace std;
using namespace arma;

int main(){
    arma_rng::set_seed_random();
    wall_clock timer;
    int samplesNumber = 256;
    vec samples = randn<vec>(samplesNumber);
    vec weights = randn<vec>(samplesNumber);
    
    timer.tic();
    double result1 = wmedian(samples, weights);
    double duration1 = timer.toc();
    
    timer.tic();
    double result2 = wmedianf(samples, weights);
    double duration2 = timer.toc();
    
    cout << "wmedian result:  " << fixed << result1 << endl;
    cout << "wmedian elapsed time:  " << scientific << duration1 << " s" << endl;
    cout << "wmedianf result: " << fixed << result2 << endl;
    cout << "wmedianf elapsed time: " << scientific << duration2 << " s" << endl;
    return 0;
}
