function [x,iter,error] = arwmr(y,A,epsilon,beta,tol,itmax,wmedianfunc)
%ARWMR Adaptive Regularization - Weighted Median Regression Algorithm
%
%   Traditional solver for the l1-LAD optimization problem based on the
%   Weighted Median operator
%
%   x = arg min ||y - Ax||_1 + lambda ||x||_1
%
%   X = ARWMR(Y,A,EPSILON,BETA,TOL,ITMAX) returns a size(A,2)-element
%   coefficient vector that represents vector Y on the vector space of A.
%
%   [X, ITER, ERROR] = ARWMR(Y,A,EPSILON,BETA,TOL,ITMAX) returns a 
%   size(A,2)-element coefficient vector that represents vector Y on the
%   vector space of A. Iteration reached by the algorithm and normalized
%   reconstruction error.
%
%   Input:
%       Y: measured signal vector
%       A: dictionary matrix
%       EPSILON: help constant to avoid division by zero
%       BETA: continuation approach Tau shrinking constant
%       TOL: minimum reconstruction tolerance value
%       ITMAX: maximum outer loop iteration number
%       WMEDIANFUNC: function handler to change inner median estimator, by
%       default @wmedian
%
%   Output:
%       X: sparse coefficient vector
%       ITER: maximum iteration reached
%       ERROR: normalized reconstruction error
%
%   Reference:
%       Robust Sparse Signal Recovery Based on Weighted Median Operator
%       http://www.mirlab.org/conference_papers/International_Conference/ICASSP%202014/papers/p1050-ramirez.pdf

if nargin == 6
    wmedianfunc = @wmedianf;
end

% Init
[M,N] = size(A);
x = zeros(N,1);
error = 1;
tau = epsilon * max(sum(abs(A)))/2;
yaux = zeros(M,1);
n2Y = norm(y)^2;
iter = 0;

% Outer Loop
while iter < itmax && error > tol
    % Inner Loop
    for k = 1:N
        Yi = [(y - yaux + A(:,k) * x(k))./A(:,k); 0];
        wi = [abs(A(:,k)); tau/(epsilon + abs(x(k)))];
        xprev = x(k);
        x(k) = wmedianfunc(Yi,wi,sum(Yi)/(M+1));
        yaux = yaux + A(:,k) * (x(k) - xprev);
    end
    error = norm(y - yaux)^2/n2Y;
    iter = iter + 1;
    tau = tau * beta;
end
