# Matlab codes

Here you can find sparse problem solvers based on the Weighted Median operator implemented for Matlab.

## Functions list

### arwmr.m  
Adaptive Regularization - Weighted Median Regression Algorithm. [1]  
Solver for the l1-LAD optimization problem.

### wmr.m  
Weighted Median Regression Algorithm. [2]  
Solver for the l0-LAD optimization problem.

### wmedian.m
Traditional Weighted Median Operator with real weights solver. [3]

# Mex functions list

### wmedianf.mexa64
Fast Weighted Median Operator for Linux amd64. [4]

## Demos

### demo1.m
Simple DCT coefficients recovery test from a noiseless signal.

### demo2.m
Simple CS framework coefficients recovery test from a noiseless signal.

### demo3.m
Script to reproduce Figure 2.a or 2.b from "Robust Sparse Signal Recovery Based on Weighted Median Operator". NMSE, in dB, of recovered coefficient vector versus SNR of measurements corrupted with additive gaussian noise or e-contaminated normal noise with 3% gross errors.

### demo4.m
Script to reproduce Figure 2.b from "Robust Sparse Signal Recovery Based on Weighted Median Operator". R-SNR of the target signal versus the impulsive level of e-contaminated additive noise and SNR 12 dB.

## References

1. [Robust Sparse Signal Recovery Based on Weighted Median Operator](http://www.mirlab.org/conference_papers/International_Conference/ICASSP%202014/papers/p1050-ramirez.pdf)
2. [Compressive Sensing Signal Reconstruction by Weighted Median Regression Estimates](http://dx.doi.org/10.1109/TSP.2011.2125958)
3. [A General Weighted Median Filter Structure Admitting Negative Weights](http://dx.doi.org/10.1109/78.735296)
4. [A fast weighted median algorithm based on Quickselect](http://dx.doi.org/10.1109/ICIP.2010.5651855)
