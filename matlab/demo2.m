% Demo - 2
% Simple CS framework coefficients recovery test from a noiseless signal

% Parameters
N = 512; % Number of coefficients
M = 256; % Number of measured projections
S = 8; % Sparsity constraint

% Coefficient selection
x = zeros(N,1);
coeff = randn(S,1);
coeffIndex = randperm(N);
x(coeffIndex(1:S)) = coeff;

% Projection Dictionary
A = randn(M, N);
A = A*(diag(1./sqrt(sum(A.^2,1))));

% Measured Signal
y = A * x;

[xHat, iter, error1] = arwmr(y, A, 0.01, 0.95, 10^-6, 15);

Error = 1/N*sum((x-xHat).^2);

fprintf('Coefficients MSE = %.4e\n', Error);

% Output plot
figure
stem([1:N;1:N]',[x,xHat],'filled');
title('Simple CS framework coefficients recovery test');
xlabel('n');
ylabel('x');
legend('Original','Estimated');