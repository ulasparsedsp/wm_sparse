% Demo - 3
% Script to reproduce Figure 2.a or 2.b from "Robust Sparse Signal Recovery
% Based on Weighted Median Operator"
% NMSE, in dB, of recovered coefficient vector versus SNR of measurements
% corrupted with additive gaussian noise or e-contaminated normal noise 
% with 3% gross errors
clear

prompt = ['This script can take a long running time.\n', ...
         'Do you want to continue? Y/N [N]: '];
str = lower(input(prompt,'s'));
if isempty(str)
    str = 'n';
end
if strcmp(str,'n')
   return 
end

display(' ')

prompt = ['Select noise type\n', '"g" for gaussian or "e" for',...
          ' e-contaminated [g]: '];
str = lower(input(prompt,'s'));

if strcmp(str,'e')
    noise_type = 'e-cont';
else
    noise_type = 'gaussian';
end

display(' ')

prompt = 'Choose the number of trials [1000]: ';
input = lower(input(prompt));

if isempty(input)
    trials = 1000;
else
    trials = input;
end
display(' ')

SNR = (0:2:26)';

fileName = ['test_CSR_ARWMR_' noise_type '_' datestr(now,'ddmmyy-HHMM')];
save(fileName, 'SNR');
N = 512;
M = 256;
S = 25;

algorithms = 2;
error = zeros(numel(SNR), algorithms);

itmax = 100;
tol = 10^-6;
beta = 0.95;

for trial = 1:trials
    fprintf('Running trial: %u\n',trial);
    for gross_index = 1:numel(SNR)
        % Coeficientes
        coeff = randn(S,1);
        coeffIndex = randperm(N);
        x = zeros(N,1);
        x(coeffIndex(1:S)) = coeff;
        
        % Diccionario
        A = randn(M, N);
        A = A*(diag(1./sqrt(sum(A.^2,1))));
        At = A';
        
        % Senal medida
        y = A * x;
        if strcmp(noise_type, 'gaussian')
            e = randn(M, 1);
            e = e * sqrt(sum(y.^2) / (sum(e.^2) * 10^(SNR(gross_index)/10)));
        elseif strcmp(noise_type, 'e-cont')
            gross_errors = 3;
            varnoise = mean(y.^2)/(10^(SNR(gross_index)/10));
            u = rand(M,1);
            e = (u > gross_errors/100).*(sqrt(varnoise)*randn(M,1)) + (u <= gross_errors/100).*(sqrt(100*varnoise)*randn(M,1));
        end
        
        y = y + e;
        
        % WMGT
        xHat = arwmr(y, A, 0.01, beta, tol, itmax);
        error(gross_index, 1) = error(gross_index, 1) + 10 * log10(sum((xHat - x).^2) / sum(x.^2));
        
        % WMHT
        xHat = wmr(y, A, beta, tol, itmax);
        error(gross_index, 2) = error(gross_index, 2) + 10 * log10(sum((xHat - x).^2) / sum(x.^2));
    end
    error_curves = error./trial;
    save(fileName, 'error_curves', '-append');
end
display('End')

% Figure
figure
plot(SNR, error_curves(:,1),'^-',SNR, error_curves(:,2),'+-')
xlabel('SNR (dB)'), ylabel('NMSE (dB)');
legend('ARWMR', 'WMR');
axis([SNR(1) SNR(end) min(min(error_curves)) max(max(error_curves))]);
