% Demo - 1
% Simple DCT coefficients recovery test from a noiseless signal

% Parameters
N = 256; % Number of coefficients
S = 8; % Sparsity constraint

% Coefficient selection
x = zeros(N,1); 
coeff = randn(S,1);
coeffIndex = randperm(N);
x(coeffIndex(1:S)) = coeff;

% DCT Projection Dictionary
A = dctmtx(N)';

% Measured Signal
y = A * x;

xHat = arwmr(y, A, 0.01, 0.95, 10^-6, 15);

Error = 1/N*sum((x-xHat).^2);

fprintf('Coefficients MSE = %.4e\n', Error)

% Output plot
figure
stem([1:N;1:N]',[x,xHat],'filled');
title('Simple DCT coefficients recovery test');
xlabel('n');
ylabel('x');
legend('Original','Estimated');
