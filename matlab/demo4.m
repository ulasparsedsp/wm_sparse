% Demo - 4
% Script to reproduce Figure 2.b from "Robust Sparse Signal Recovery Based
% on Weighted Median Operator"
% R-SNR of the target signal versus the impulsive level of e-contaminated
% additive noise and SNR 12 dB

prompt = ['This script can take a long running time.\n', ...
         'Do you want to continue? Y/N [N]: '];
str = lower(input(prompt,'s'));
if isempty(str)
    str = 'n';
end

if strcmp(str,'n')
   return
end

display(' ')

prompt = 'Choose the number of trials [1000]: ';
input = lower(input(prompt));

if isempty(input)
    trials = 1000;
else
    trials = input;
end

SNR = 12;
gross_error = (0:1:10)';

fileName = ['test_CSR_Impulsiveness_' datestr(now,'ddmmyy-HHMM')];
save(fileName, 'SNR', 'gross_error');

N = 512;
M = 256;
S = 25;

algorithms = 5;
error = zeros(numel(gross_error), algorithms);

itmax = 100;
tol = 10^-6;
beta = 0.95;

for trial = 1:trials
    fprintf('Running trial: %u\n',trial);
    for gross_index = 1:numel(gross_error)
        % Coeficientes
        coeff = randn(S,1);
        coeffIndex = randperm(N);
        x = zeros(N,1);
        x(coeffIndex(1:S)) = coeff;

        % Diccionario
        A = randn(M, N);
        A = A*(diag(1./sqrt(sum(A.^2,1))));
        At = A';

        % Senal medida
        y = A * x;
        varnoise = mean(y.^2)/(10^(SNR/10));
        u = rand(M,1);
        e = (u > gross_error(gross_index)/100).*(sqrt(varnoise)*randn(M,1)) + (u <= gross_error(gross_index)/100).*(sqrt(100*varnoise)*randn(M,1));
        y = y + e;

        % WMGT
        xHat = arwmr(y, A, 0.01, beta, tol, itmax);
        error(gross_index, 1) = error(gross_index, 1) + 10 * log10(sum((xHat - x).^2) / sum(x.^2));

        % WMHT
        xHat = wmr(y, A, beta, tol, itmax);
        error(gross_index, 2) = error(gross_index, 2) + 10 * log10(sum((xHat - x).^2) / sum(x.^2));

    end
    error_curves = -error./trial;
    save(fileName, 'error_curves', '-append');
end
display('End')

% Figure
figure
plot(gross_error, error_curves(:,1),'^-',gross_error, error_curves(:,2),'+-')
xlabel('Impulsividad \epsilon (%)'), ylabel('R-SNR (dB)');
legend('ARWMR', 'WMR','Location','southwest');
axis([gross_error(1) gross_error(end) min(min(error_curves)) max(max(error_curves))]);
