function [x,iter,error] = wmr(y,A,beta,tol,itmax,wmedianfunc)
%WMR Adaptive Regularization - Weighted Median Regression Algorithm
%
%   Traditional solver for the l0-LAD optimization problem based on the
%   Weighted Median operator
%
%   x = arg min ||y - Ax||_1 + lambda ||x||_0
%
%   X = WMR(Y,A,BETA,TOL,ITMAX) returns a size(A,2)-element
%   coefficient vector that represents vector Y on the vector space of A.
%
%   [X, ITER, ERROR] = WMR(Y,A,BETA,TOL,ITMAX) returns a 
%   size(A,2)-element coefficient vector that represents vector Y on the
%   vector space of A. Iteration reached by the algorithm and normalized
%   reconstruction error.
%
%   Input:
%       Y: measured signal vector
%       A: dictionary matrix
%       BETA: continuation approach Threshold shrinking constant
%       TOL: minimum reconstruction tolerance value
%       ITMAX: maximum outer loop iteration number
%       WMEDIANFUNC: function handler to change inner median estimator, by
%       default @wmedian
%
%   Output:
%       X: sparse coefficient vector
%       ITER: maximum iteration reached
%       ERROR: normalized reconstruction error
%
%   Reference:
%       Compressive Sensing Signal Reconstruction by Weighted Median 
%       Regression Estimates
%       http://dx.doi.org/10.1109/TSP.2011.2125958

if nargin == 5
    wmedianfunc = @wmedianf;
end

% Init
[M,N] = size(A);
error = tol + 1;
thr = max(abs(A'*y));
x = zeros(N,1);
yaux = zeros(M,1);
n2Y = norm(y)^2;
iter = 0;

% Outer Loop
while iter < itmax && error > tol
    % Inner Loop
    for k = 1:N
        rn = y - yaux + A(:,k) * x(k);
        w = abs(A(:,k));
        xTemp = wmedianfunc(rn./A(:,k),w,sum(rn./A(:,k))/M);
        xprev = x(k);
        rt = sum(abs(rn) - abs(rn - A(:,k)*xTemp));
        x(k) = (rt > thr)*xTemp;
        yaux = yaux + A(:,k) * (x(k) - xprev);
    end
    error = norm(y - yaux)^2/n2Y;
    iter = iter + 1;
    thr = thr * beta;
end