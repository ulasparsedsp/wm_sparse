# -*- coding: utf-8 -*-
"""
Module for Weighted Median Regression solver functions
"""
import numpy as np
from numpy import linalg as LA
import wmedian as wm

def wmr(y, A, beta=0.95, tol=1e-6, itmax=15, nargout=1, **kwargs):
    """
    Traditional solver for the l0-LAD optimization problem based on the
    Weighted Median operator

    x = arg min ||y - Ax||_1 + lambda ||x||_0
    
    X = WMR(Y, A, BETA, TOL, ITMAX) returns a A.shape[1]-element
    coefficient vector that represents vector Y on the vector space of A.
    
    X, ITERATION, ERROR = WMR(Y, A, BETA, TOL, ITMAX, NARGOUT)
    returns a A.shape[1]-element coefficient vector that represents vector
    Y on the vector space of A. Iteration reached by the algorithm and
    normalized reconstruction error.
    
    Parameters
    ----------
    y : ndarray
        Measured signal 1-D vector.
        
    A : ndarray
        Measurement matrix or Holographic Dictionary.
        
    beta : float
        Continuation approach Tau shrinking constant (default is 0.95).
        
    tol : float
        Minimum reconstruction tolerance value (default is 1e-6).
        
    itmax : int
        Maximum outer loop iteration number (default is 15).
        
    nargout : int, optional
        Number of outputs (default 1: returns estimated coefficiente vector; 2:
        returns vector and maximum iteration reached; 3: returns vector, 
        iteration and normalized reconstruction error).
        
    wmedianFn : function object, optional
        Inner weighted median estimator function (default wm.wmedianf without
        additional arguments). If needed must be passed by keyword argument.
        
    fwmedianFn : function object, optional
        Inner Cython-based fast weighted median estimator function (default 
        cywm.wmedianf in wm.wmedianf). This works only if wm.wmedianf is passed
        as 'wmedianfunc'. If needed must be passed by keyword argument.
    
    Returns
    -------
    x : ndarray
        Sparse coefficient vector.
        
    iteration : int
        Maximum iteration reached.
        
    error : float
        Normalized reconstruction error.

    References
    ----------
    [1] Compressive Sensing Signal Reconstruction by Weighted Median 
        Regression Estimates
        http://dx.doi.org/10.1109/TSP.2011.2125958
    """
    
    wmedianFn = kwargs.get('wmedianFn', wm.wmedianf)
    
    # Init
    M, N = A.shape
    x = np.zeros(N)
    error = tol + 1
    thr = np.max(np.abs(A.T.dot(y)))
    yaux = np.zeros(M)
    iteration = 0
    n2Y = LA.norm(y)**2
    
    # Outer Loop
    while (iteration < itmax) and (error > tol):
        # Inner Loop
        for k in range(N):
            rn = y - yaux + A[:, k] * x[k]
            w = np.abs(A[:, k])
            xTemp = wmedianFn(rn/A[:,k], w, **kwargs)
            xprev = x[k]
            rt = np.sum(np.abs(rn) - np.abs(rn - A[:, k] * xTemp))
            x[k] = (rt > thr)*xTemp
            yaux = yaux + A[:, k] * (x[k] - xprev)
            
        error = LA.norm(y - yaux)**2/n2Y
        iteration += 1
        thr *= beta
    
    if nargout == 1:
        return x
    elif nargout == 2:
        return x, iteration
    else:
        return x, iteration, error
        
if __name__ == '__main__':
    
    import matplotlib.pyplot as plt
    
    # Simple CS framework coefficients recovery test from a noiseless signal 
    N = 512
    M = 256
    S = 8
    
    x = np.zeros(N)
    coeff = np.random.randn(S)
    coeffIndex = np.random.permutation(N)
    x[coeffIndex[:S]] = coeff
    
    A = np.random.randn(M, N)
    A *= (1/np.sqrt((A**2).sum(0)))
    
    y = A.dot(x)
    
    xHat, iteration, error = wmr(y, A, nargout=3)
    print 'Iteration reached: {}'.format(iteration)
    print 'Error achieved: {}'.format(error)
    
    plt.figure()
    plt.stem(x, markerfmt='bo')
    plt.stem(xHat, linefmt='r', markerfmt='ro')
    plt.title('Coefficients recovery using WMR')
    plt.xlabel('n')
    plt.ylabel('x')
    plt.show()