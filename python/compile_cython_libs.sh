#!/bin/bash
FILE_NAME="setup.py"
BUILD_DIR="build"
clear
echo "Running $FILE_NAME with recommended configuration..."
python $FILE_NAME build_ext --inplace
echo "Done."
echo "Removing $BUILD_DIR directory..."
rm -rf $BUILD_DIR
echo "Done."
