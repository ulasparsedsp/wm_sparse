# -*- coding: utf-8 -*-
"""
Module for Weighted Median Operator functions
"""

import numpy as np
import cywmedian as cywm


def wmedian(samples, weights=None, **kwargs):
    """
    Traditional Weighted Median Operator with real weights function

    X = WMEDIAN(SAMPLES) returns the weighted median of SAMPLES with
    unitary weights, or the traditional median without interpolation.

    X = WMEDIAN(SAMPLES, WEIGHTS) returns the weighted median of SAMPLES
    weighted by WEIGHTS.

    X, I = WMEDIAN(SAMPLES, WEIGHTS) returns the weighted median of SAMPLES
    weighted by WEIGHTS in X and the median index I.

    Parameters
    ----------
    samples : ndarray
        Vector or matrix of samples.

    weights : ndarray
        Vector or matrix of weights.

    index : bool, optional
        Enables median index return (default False). If needed, must be passed 
        by keyword argument.

    Returns
    -------
    samples[median_index] : dtype of samples
        Computed weighted median value

    median_index : int
        Computed weighted median index

    References
    ----------
    [1] A General Weighted Median Filter Structure Admitting Negative Weights
        http://dx.doi.org/10.1109/78.735296
    """

    if weights is None:
        weights = np.ones_like(samples)
        
    if len(samples.shape) > 1:
        samples = samples.flatten()
        weights = weights.flatten()
        
    samples = samples * (np.sign(weights) + (weights == 0))
    weights = np.abs(weights)
    
    sorted_index = samples.argsort()[::-1]
    partialSum = np.cumsum(weights[sorted_index])
    threshold = weights.sum()/2.0
    
    median_index = sorted_index[partialSum >= threshold][0]
    
    if kwargs.get('index', False):
        return samples[median_index], median_index
    else:
        return samples[median_index]
        
   
def wmedianf(samples, weights=None, **kwargs):
    """
    Fast Weighted Median Operator with real weights function
    This function is built upon Cython-based Weighted Median Functions

    X = WMEDIANF(SAMPLES) returns the weighted median of SAMPLES with
    unitary weights, or the traditional median without interpolation.

    X = WMEDIANF(SAMPLES, WEIGHTS) returns the weighted median of SAMPLES
    weighted by WEIGHTS.

    X, I = WMEDIANF(SAMPLES, WEIGHTS) returns the weighted median of SAMPLES
    weighted by WEIGHTS in X and the median index I.

    Parameters
    ----------
    samples : ndarray, dtype=np.double
        Vector or matrix of samples.

    weights : ndarray, dtype=np.double
        Vector or matrix of weights.
    
    fwmedianFn : function object, optional
        Fast weighted median estimator function (default Cython-based 
        cywm.wmedianf)
        You can pass every function defined in cywmedian module as keyword
        argument.

    index : bool, optional
        Enables median index return (default False). If needed, must be passed 
        by keyword argument.

    Returns
    -------
    samples[median_index] : np.double
        Computed weighted median value

    median_index : np.int
        Computed weighted median index
    """
    
    fwmedianFn = kwargs.get('fwmedianFn', cywm.wmedianf)    
    
    if weights is None:
        weights = np.ones_like(samples)
        
    if len(samples.shape) > 1:
        samples = samples.flatten()
        weights = weights.flatten()
        
    median, median_index = fwmedianFn(samples, weights)
        
    if kwargs.get('index', False):
        return median, median_index
    else:
        return median
    return
    
if __name__ == '__main__':
    # Simple test for functions
    n = np.random.randint(512, 1024)
    #y = np.random.permutation(n).astype(np.float)
    y = np.random.randn(n)
    w = np.random.randn(n)
    
    print 'Number of samples: {}'.format(n)
    print 'Median: {}'.format(wmedian(y, w))
    print 'Median: {0[0]}; Index: {0[1]}\n'.format(wmedian(y, w, index=True))
    
    print 'Number of samples: {}'.format(n)
    print 'Median: {}'.format(wmedianf(y, w))
    print 'Median: {0[0]}; Index: {0[1]}\n'.format(wmedian(y, w, index=True))
    
    print 'Number of samples: {}'.format(n)
    print 'Median: {}'.format(wmedianf(y, w, fwmedianfunc=cywm.wmedian))
    print 'Median: {0[0]}; Index: {0[1]}\n'.format(wmedian(y, w, index=True))
    