# Python codes

Here you can find sparse problem solvers based on the Weighted Median operator implemented for Python 2.7.x.

## Modules list

### arwmr.py
Adaptive Regularization - Weighted Median Regression Algorithm. [1]  
Solver for the l1-LAD optimization problem.

* **arwmr** : Traditional solver for the l1-LAD optimization problem based on the Weighted Median operator.

### wmr.py
Weighted Median Regression Algorithm. [2]  
Solver for the l0-LAD optimization problem.

* **wmr** : Traditional solver for the l0-LAD optimization problem based on the Weighted Median operator.

### wmedian.py
Weighted Median Operator with real weights solvers. [3]

* **wmedian** : Traditional Weighted Median Operator with real weights solver.
* **wmedianf** : Fast Weighted Median Operator with real weights solver. This function performs calls to Cython-based functions of **cywmedian.pyx**.

## Cython modules list

### cywmedian.pyx
Cython-based Functions for the Weighted Median Operator.

* **wmedian** : Cython-based Weighted Median Operator with real weights solver.
* **wmedianf** : Fast Cython-based Weighted Median Operator with real weights solver.

## Scripts

### compile_cython_libs.sh
Bash script to run Python compilation scripts for Cython modules. Run this script in a Linux command prompt by doing:
```
chmod u+x compile_cython_libs.sh
./compile_cython_libs.sh
```

### setup.py
Python script to compile Cython modules. Beside *compile_cython_libs.sh*, you can compile Cython modules directly from this script in a Linux command prompt by doing:
```
python setup.py build_ext --inplace
```

## References

1. [Robust Sparse Signal Recovery Based on Weighted Median Operator](http://www.mirlab.org/conference_papers/International_Conference/ICASSP%202014/papers/p1050-ramirez.pdf)
2. [Compressive Sensing Signal Reconstruction by Weighted Median Regression Estimates](http://dx.doi.org/10.1109/TSP.2011.2125958)
3. [A General Weighted Median Filter Structure Admitting Negative Weights](http://dx.doi.org/10.1109/78.735296)

## Notes
* We strongly recommend using a Python distribution like [Anaconda](https://www.continuum.io/downloads#_unix) if you are not skilled about NumPy, Cython and Matplotlib modules installation and configuration alongside with Python. Anaconda [*version>=2.5*] includes the Intel Math Kernel Library (MKL) optimizations for improved performance, for free to use.
