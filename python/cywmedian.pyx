# -*- coding: utf-8 -*-
"""
Cython-based Functions for the Weighted Median Operator
"""
import cython
cimport cython

import numpy as np
cimport numpy as np

FLOAT = np.double
ctypedef np.double_t FLOAT_t

INT = np.int
ctypedef np.int_t INT_t
    

@cython.boundscheck(False)
def wmedian(np.ndarray[FLOAT_t] samples, np.ndarray[FLOAT_t] weights):
    """
    Cython-based Weighted Median Operator
    Naive approach of wmedian.wmedian with type definitions and replacing
    numpy.cumsum by for loop
    """
    
    samples = samples * (np.sign(weights) + (weights == 0))
    weights = np.abs(weights)
    
    cdef np.ndarray[INT_t] sorted_index = samples.argsort()[::-1]
    cdef FLOAT_t threshold = weights.sum()/2.0
    
    cdef FLOAT_t partialSum = 0
    cdef Py_ssize_t i
    cdef INT_t median_index
    
    for i in xrange(sorted_index.size):
        partialSum += weights[sorted_index[i]]
        if partialSum >= threshold:
            median_index = sorted_index[i]
            break
    
    return samples[median_index], median_index
    

@cython.boundscheck(False)
def wmedianf(np.ndarray[FLOAT_t] samples, np.ndarray[FLOAT_t] weights):
    """
    Cython-based Weighted Median Operator
    Median search from Pivot-Mean approach
    """
    
    samples = samples * (np.sign(weights) + (weights == 0))   
    weights = np.abs(weights)
    
    cdef np.ndarray[INT_t] sorted_index = samples.argsort()[::-1]
    cdef FLOAT_t threshold = 0
    cdef FLOAT_t pivot = 0
    cdef FLOAT_t w1 = 0
    cdef FLOAT_t w2 = 0
    cdef INT_t N1 = 0
    cdef INT_t median_index
    cdef Py_ssize_t i
    
    for i in range(samples.size):
        pivot += samples[i]
        threshold += weights[i]
    
    pivot /= samples.size
    threshold /= 2.0
    
    for i in range(samples.size):
        if samples[i] > pivot:
            w1 += weights[i]
            N1 += 1
        else:
            w2 += weights[i]
    
    cdef FLOAT_t partialSum
    
    if w1 > w2:
        i = N1
        partialSum = w1
        
        while partialSum >= threshold:
            i -= 1
            partialSum -= weights[sorted_index[i]]
    else:
        i = N1 - 1
        partialSum = w1
        
        while partialSum < threshold:
            i += 1
            partialSum += weights[sorted_index[i]]
        
    median_index = sorted_index[i]
        
    return samples[median_index], median_index

