# -*- coding: utf-8 -*-
"""
Setup File for Cython-based Weighted Median Operator Functions
"""

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy

extensions = [Extension("cywmedian",["cywmedian.pyx"])]
setup(
    name = 'cywmedian',
    ext_modules = cythonize(extensions),
    include_dirs = [numpy.get_include()] 
)